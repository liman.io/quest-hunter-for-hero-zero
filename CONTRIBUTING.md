Feel free [to open issues](https://gitlab.com/liman.io/quest-hunter-for-hero-zero/-/issues/new) for feature requests or bug reports, merge requests are always welcome as well.
