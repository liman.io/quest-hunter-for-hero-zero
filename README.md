# Hero Zero: Quest Hunter

![](https://hz-static-landing.akamaized.net/images/header.jpg)

This is an extension for Google Chrome to determine the most-rewarding quest to complete in the online
game [Hero Zero](https://herozerogame.com).

## Installation

This extension is currently not available on the Chrome web store. Hence, to install this extension follow these steps:

1. Download this Google Chrome extension
   as [`hero-zero-quest-hunter-1.1.2.zip`](https://gitlab.com/liman.io/quest-hunter-for-hero-zero/-/package_files/23808599/download)
   .
2. Extract the `dist` folder from `hero-zero-quest-hunter-1.1.2.zip`.
3. In Google Chrome, navigate to `chrome://extensions` and activate the developer
   mode: ![](docs/Developer%20Mode%20in%20Google%20Chrome%20Extensions.png)
4. Load the extension through Google Chrome by selecting the previously extracted
   folder: ![](docs/Loading%20an%20Unpacked%20Google%20Chrome%20Extension.png)

After completing the above steps, you have successfully installed and activated the Google Chrome extension. Optionally,
you can now deactivate the developer mode, again.

## Usage

1. In Google Chrome, pin this extension: ![](docs/Pinning%20the%20Google%20Chrome%20Extension.png)
2. Open Hero Zero in Google Chrome.
3. Open the extension by clicking on the button with the quest icon in the lower left corner and observe the available
   quests being listed there: ![](docs/Extension.png)

### User Interface of the Extension

![](docs/Table.png)

The user interface consists of a table with multiple columns which list the following information:

- `Stage (Position)`: the number of the stage where a quest is located and its position on that stage
- `Type`: the type (fight or time) of a quest
- `Reward`: the coins rewarded per energy unit and in total as well as the experience rewarded per energy unit and in
  total
- `Cost`: the energy required in total as well as the time required in minutes

**To determine the most-rewarding quest to complete, sort the column of the desired reward in descending order.** Due to
space limitations, quests are listed on multiple pages that you can browse through using the numbers on the lower end.

If you encounter any issues while using this extension, refer to [the next section](#troubleshooting).

## Troubleshooting

### The extension is loading indefinitely or displays incorrect data about the quests

Reload the extension through `chrome://extensions`: ![](docs/Reload%20the%20Google%20Chrome%20Extension.png) and reload
the web page of Hero Zero.

### The issue I encountered is not listed here

I am sorry about that, feel free to [open an issue](https://gitlab.com/liman.io/quest-hunter-for-hero-zero/-/issues/new)
explaining your circumstances and the problem you encountered.

## Roadmap

- [ ] Use the names of stages rather than their number
- [x] Integrate the user interface of this extension with the user interface of Hero Zero
