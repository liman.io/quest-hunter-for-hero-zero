chrome.webRequest.onBeforeRequest.addListener(
  (request) => {
    const formData = request.requestBody?.formData;

    if (formData) {
      const [action] = formData["action"];

      if (action === "autoLoginUser") {
        chrome.storage.local.set({ formData });
        chrome.storage.local.set({ loginRequestID: request.requestId });
      } else if (action === "claimQuestRewards") {
        chrome.storage.local.set({
          questCompletionRequestID: request.requestId,
        });
      }
    }
  },
  { urls: ["https://*.herozerogame.com/request.php"] },
  ["requestBody"]
);

chrome.webRequest.onCompleted.addListener(
  (response) => {
    chrome.storage.local.get(
      ["formData", "loginRequestID", "questCompletionRequestID"],
      (result) => {
        const formData = result["formData"];
        const loginRequestID = result["loginRequestID"];
        const questCompletionRequestID = result["questCompletionRequestID"];

        if (response.requestId === loginRequestID) {
          chrome.tabs.query(
            { active: true, url: "https://*.herozerogame.com/*" },
            function (tabs) {
              chrome.tabs.sendMessage(tabs[0].id, {
                action: "loginCompleted",
                formData,
              });
            }
          );
        } else if (response.requestId === questCompletionRequestID) {
          chrome.tabs.query(
            { active: true, url: "https://*.herozerogame.com/*" },
            function (tabs) {
              chrome.tabs.sendMessage(tabs[0].id, { action: "questsUpdated" });
            }
          );
        }
      }
    );
  },
  {
    urls: ["https://*.herozerogame.com/request.php"],
  }
);

/*
 * Due to a bug (refer to https://bugs.chromium.org/p/chromium/issues/detail?id=1152255), this service worker becomes
 * inactive after five minutes.
 */
chrome.alarms.create({ periodInMinutes: 4 });
chrome.alarms.onAlarm.addListener(() => {
  console.log(
    "Alarm necessary to keep background service worker from becoming inactive."
  );
});
