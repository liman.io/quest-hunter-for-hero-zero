import { Table } from "antd";
import { AlignType } from "rc-table/lib/interface";
import { Coin, Energy, Experience, Fight, Time } from "./Icons";
import Quest from "./Quest";

const round = (x: number) => Math.round((x + Number.EPSILON) * 100) / 100;

const QuestTable = ({ quests }: { quests: Quest[] }) => {
  const columns = [
    {
      align: "center" as AlignType,
      dataIndex: "stage",
      render: (text: any, record: Quest) => `${text} (${record.position})`,
      title: "Stage (Position)",
    },
    {
      align: "center" as AlignType,
      dataIndex: "type",
      filterMultiple: false,
      filters: [
        { text: <Time />, value: 1 },
        { text: <Fight />, value: 2 },
      ],
      onFilter: (value: boolean | number | string, record: Quest) =>
        record.type === value,
      render: (text: any, record: Quest) =>
        record.type === 1 ? <Time /> : <Fight />,
      title: "Type",
    },
    {
      children: [
        {
          dataIndex: ["reward", "coins"],
          render: (text: any, record: Quest) =>
            `${round(record.reward.coins / record.cost.energy)} (${text})`,
          sortDirections: ["descend"],
          sorter: (firstQuest: Quest, secondQuest: Quest) =>
            firstQuest.reward.coins / firstQuest.cost.energy -
            secondQuest.reward.coins / secondQuest.cost.energy,
          title: (
            <>
              <Coin /> / <Energy /> (∑)
            </>
          ),
        },
        {
          dataIndex: ["reward", "experience"],
          render: (text: any, record: Quest) =>
            `${round(record.reward.experience / record.cost.energy)} (${text})`,
          sortDirections: ["descend"],
          sorter: (firstQuest: Quest, secondQuest: Quest) =>
            firstQuest.reward.experience / firstQuest.cost.energy -
            secondQuest.reward.experience / secondQuest.cost.energy,
          title: (
            <>
              <Experience /> / <Energy /> (∑)
            </>
          ),
        },
      ],
      title: "Reward",
    },
    {
      children: [
        {
          align: "center" as AlignType,
          dataIndex: ["cost", "energy"],
          sorter: (firstQuest: Quest, secondQuest: Quest) =>
            firstQuest.cost.energy - secondQuest.cost.energy,
          title: <Energy />,
        },
        {
          align: "center" as AlignType,
          dataIndex: ["cost", "time"],
          render: (text: any, record: Quest) => record.cost.time / 60,
          sorter: (firstQuest: Quest, secondQuest: Quest) =>
            firstQuest.cost.time - secondQuest.cost.time,
          title: (
            <>
              <Time marginRight={5} /> (in min.)
            </>
          ),
        },
      ],
      title: "Cost",
    },
  ];

  return (
    <Table
      bordered={true}
      columns={columns}
      dataSource={quests}
      pagination={{ pageSize: 6, position: ["bottomCenter"] }}
      size="small"
      title={() => <b>Quests</b>}
    />
  );
};

export default QuestTable;
