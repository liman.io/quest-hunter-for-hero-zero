import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";

const extensionContainer = document.createElement("div");
extensionContainer.id = "hero-zero-quest-hunter-container";

const [mainContainer] = document.getElementsByClassName("main-container");
if (mainContainer) mainContainer.appendChild(extensionContainer);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  extensionContainer
);
