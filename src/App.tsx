import { Button, Popover } from "antd";
import { useEffect, useState } from "react";
import telephoneIcon from "../static/telephone.png";
import Quest, { parseQuests } from "./Quest";
import QuestTable from "./QuestTable";

const useQuests = () => {
  const [loginFormData, setLoginFormData] =
    useState<Record<string, [string]>>();
  const [quests, setQuests] = useState<Quest[]>([]);
  const [shouldUpdateQuests, setShouldUpdateQuests] = useState<boolean>(true);

  chrome.runtime.onMessage.addListener(
    async (request: {
      action: string;
      formData?: Record<string, [string]>;
    }) => {
      if (request.action === "loginCompleted") {
        setLoginFormData(request.formData);
      } else if (request.action === "questsUpdated") {
        setShouldUpdateQuests(true);
      }
    }
  );

  useEffect(() => {
    if (!shouldUpdateQuests || !loginFormData) return;

    const formData = new FormData();
    Object.entries(loginFormData).forEach(([key, value]) =>
      formData.append(key, value[0])
    );

    fetch(window.location.href + "request.php", {
      body: formData,
      method: "POST",
    })
      .then((response) => response.json())
      .then((playerData) => setQuests(parseQuests(playerData.data.quests)));

    setShouldUpdateQuests(false);
  }, [loginFormData, shouldUpdateQuests]);

  return quests;
};

const App = () => {
  const quests = useQuests();

  return (
    <Popover
      content={() => <QuestTable quests={quests} />}
      overlayInnerStyle={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
      placement="topRight"
      trigger={quests.length === 0 ? [] : "click"}
    >
      <Button
        icon={
          <img
            alt="Telephone"
            src={chrome.runtime.getURL(telephoneIcon)}
            style={{ height: 32, marginTop: 7, width: 32 }}
          />
        }
        loading={quests.length === 0}
        shape="circle"
        size="large"
        style={{
          bottom: 25,
          boxShadow: "grey 0 0 10px",
          height: 48,
          left: 25,
          position: "fixed",
          width: 48,
        }}
        type="primary"
      />
    </Popover>
  );
};

export default App;
