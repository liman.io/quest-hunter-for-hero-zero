import coinIcon from "../static/coin.png";
import energyIcon from "../static/energy.png";
import experienceIcon from "../static/experience.png";
import fightIcon from "../static/fight.png";
import timeIcon from "../static/time.png";

const iconStyle = { height: 16, width: 16 };

export const Coin = () => (
  <img alt="Coin" src={chrome.runtime.getURL(coinIcon)} style={iconStyle} />
);
export const Energy = () => (
  <img alt="Energy" src={chrome.runtime.getURL(energyIcon)} style={iconStyle} />
);
export const Experience = () => (
  <img
    alt="Experience"
    src={chrome.runtime.getURL(experienceIcon)}
    style={iconStyle}
  />
);
export const Fight = () => (
  <img alt="Fight" src={chrome.runtime.getURL(fightIcon)} style={iconStyle} />
);
export const Time = ({ marginRight = 0 }: { marginRight?: number }) => (
  <img
    alt="Time"
    src={chrome.runtime.getURL(timeIcon)}
    style={{ ...iconStyle, marginRight }}
  />
);
