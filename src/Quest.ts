export default interface Quest {
  stage: number;
  position: number;
  type: number;
  cost: {
    energy: number;
    time: number;
  };
  reward: {
    coins: number;
    experience: number;
  };
}

export const parseQuests = (quests: Record<string, any>[]): Quest[] => {
  const questsPerStage = 3;
  const groupedQuests = [...Array(quests.length / questsPerStage)].map((_) =>
    quests.splice(0, questsPerStage)
  );

  groupedQuests.map((group) => {
    const stages: number[] = group.map((quest) => quest["stage"]);
    let stageOfGroup: number | string = Math.max(...stages);

    if (stageOfGroup === 0) stageOfGroup = "?";

    group.forEach((quest) => (quest["stage"] = stageOfGroup));
  });

  quests = groupedQuests.flat().sort((firstQuest, secondQuest) => {
    const firstStage = firstQuest["stage"];
    const secondStage = secondQuest["stage"];

    if (typeof firstStage === "string") {
      // Quests without a known stage should appear first.
      return -1;
    } else if (typeof secondStage === "string") {
      // Quests without a known stage should appear first.
      return 1;
    } else {
      return firstStage - secondStage;
    }
  });

  return quests.map((quest, index) => {
    const parsedRewards = JSON.parse(quest["rewards"]);

    return {
      stage: quest["stage"],
      position: (index % questsPerStage) + 1,
      type: quest["type"],
      cost: {
        energy: quest["energy_cost"],
        time: quest["duration"],
      },
      reward: {
        coins: parsedRewards.coins,
        experience: parsedRewards.xp,
      },
    };
  });
};
